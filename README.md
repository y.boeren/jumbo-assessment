# Jumbo Assessment

This repository contains code to deploy a application that generates and persist data to S3 based on input variables. For user access is Cloudfront configured.

## Technical info
  - The High Level Design ![](jumbo-assessment-hld.png)
  - `S3 Bucket` to hold the output file of the pesist-data lambda function
  - `Lambda Function` to generate-data based on input variables.
  - `Lambda Function` to persist-data which takes care of persisting the data to S3
  - `Step Functions` Used for comminucation between the generate-data and persist-data lambda functions.
  - `Cloudfront` CDN for accesing the static data on S3. 

## How the application works
  - Add a name,age and city to the `variables.tf` this will be used by the application and will be provided by Cloudfront in the end.

## Local Run

After setting up the AWS credentials the following command can be used for a terraform plan:

`terraform plan -var-file secrets.tfvars`