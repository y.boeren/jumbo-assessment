resource "aws_iam_role" "iam-for-sfn" {
  name = "iam_for_sfn_role"
  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Service": "states.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}
EOF
}

resource "aws_iam_policy" "sfn-invoke-policy" {
  name = "iam-sfn-invoke-policy"
  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "lambda:InvokeFunction"
            ],
            "Resource": [
                "${aws_lambda_function.generate-data.arn}",
                "${aws_lambda_function.persist-data.arn}"
            ]
        }
    ]
})
}

resource "aws_iam_role_policy_attachment" "sfn-invoke-attachment" {
  role       = aws_iam_role.iam-for-sfn.name
  policy_arn = aws_iam_policy.sfn-invoke-policy.arn
}

resource "aws_sfn_state_machine" "sfn_state_machine" {
  name     = "demo-jumbo-sfn"
  role_arn = aws_iam_role.iam-for-sfn.arn

  definition = <<EOF
{
  "Comment": "A description of my state machine",
  "StartAt": "Lambda Invoke generate-data",
  "States": {
    "Lambda Invoke generate-data": {
      "Type": "Task",
      "Resource": "arn:aws:states:::lambda:invoke",
      "OutputPath": "$.Payload",
      "Parameters": {
        "FunctionName": "${aws_lambda_function.generate-data.arn}",
        "Payload.$": "$"
      },
      "Retry": [
        {
          "ErrorEquals": [
            "Lambda.ServiceException",
            "Lambda.AWSLambdaException",
            "Lambda.SdkClientException"
          ],
          "IntervalSeconds": 2,
          "MaxAttempts": 6,
          "BackoffRate": 2
        }
      ],
      "Next": "Lambda Invoke persist-data"
    },
    "Lambda Invoke persist-data": {
      "Type": "Task",
      "Resource": "arn:aws:states:::lambda:invoke",
      "Parameters": {
        "Payload.$": "$",
        "FunctionName": "${aws_lambda_function.persist-data.arn}"
      },
      "Retry": [
        {
          "ErrorEquals": [
            "Lambda.ServiceException",
            "Lambda.AWSLambdaException",
            "Lambda.SdkClientException"
          ],
          "IntervalSeconds": 2,
          "MaxAttempts": 6,
          "BackoffRate": 2
        }
      ],
      "End": true
    }
  }
}
EOF
}

resource "null_resource" "start_sfn" {
  provisioner "local-exec" {
    command = "aws stepfunctions start-execution --state-machine-arn ${aws_sfn_state_machine.sfn_state_machine.arn}"
  }   
}