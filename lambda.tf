data "aws_iam_policy" "LambdaBasicExecutionRole" {
  arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_role_policy_attachment" "generate-data-lambda-basic-exection" {
  role       = aws_iam_role.iam-for-lambda-generate-data.name
  policy_arn = data.aws_iam_policy.LambdaBasicExecutionRole.arn
}

resource "aws_iam_role" "iam-for-lambda-generate-data" {
  name = "iam_for_lambda_generate_data"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_lambda_function" "generate-data" {
  filename         = "src/generate_data.zip"
  function_name    = "generate_data"
  role             = aws_iam_role.iam-for-lambda-generate-data.arn
  handler          = "generate_data.lambda_handler"
  source_code_hash = filebase64sha256("src/generate_data.zip")

  runtime = "python3.9"

  environment {
    variables = {
      NAME = var.name
      AGE  = var.age 
      CITY = var.city
    }
  }
}

resource "aws_iam_role" "iam-for-lambda-persist-data" {
  name = "iam_for_lambda_persist_data"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "persit-data-lambda-basic-exection-attachment" {
  role       = aws_iam_role.iam-for-lambda-persist-data.name
  policy_arn = data.aws_iam_policy.LambdaBasicExecutionRole.arn
}

resource "aws_iam_role_policy_attachment" "persit-data-attachment" {
  role       = aws_iam_role.iam-for-lambda-persist-data.name
  policy_arn = aws_iam_policy.persist-data-policy.arn
}

resource "aws_iam_policy" "persist-data-policy" {
  name = "iam-persist-data-policy"
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "s3:ListBucket",
          "s3:PutObject"
        ],
        "Resource" : [
          "${aws_s3_bucket.s3-output-bucket.arn}",
          "${aws_s3_bucket.s3-output-bucket.arn}/*"
        ]
      }
    ]
  })
}

resource "aws_lambda_function" "persist-data" {
  filename         = "src/persist_data.zip"
  function_name    = "persist_data"
  role             = aws_iam_role.iam-for-lambda-persist-data.arn
  handler          = "persist_data.lambda_handler"
  source_code_hash = filebase64sha256("src/persist_data.zip")

  runtime = "python3.9"

  environment {
    variables = {
      BUCKET_NAME          = "${aws_s3_bucket.s3-output-bucket.id}"
      OUTPUT_PATH_FILENAME = "/tmp/"
      OUTPUT_FILENAME      = "index.html"
    }
  }
}