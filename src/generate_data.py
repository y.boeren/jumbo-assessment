import os


def lambda_handler(event, context):
    # Input parameters
    Name = os.environ.get("NAME", "Yoël")
    Age = os.environ.get("AGE", "99")
    City = os.environ.get("CITY", "Veghel")

    # Return parameters
    return {
        'Name': Name,
        'Age': Age,
        'City': City
    }

