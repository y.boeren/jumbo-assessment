import os
import boto3


def lambda_handler(event, context):
    # Input parameters
    Name = event["Name"]
    Age = event["Age"]
    City = event["City"]
    bucket_name = os.environ.get("BUCKET_NAME", "assessment-jumbo-output-bucket")
    object_dir_upload = os.environ.get("OUTPUT_PATH_FILENAME", "/tmp/")
    object_name_upload = os.environ.get("OUTPUT_FILENAME", "index.html")
    object_full_path = object_dir_upload + object_name_upload

    # Write content to file
    file = open(object_full_path, "w")
    file.write("Name = " + Name + "\n" + "Age = "+Age + "\n"+"City = "+City)
    file.close
    file = open(object_full_path, "r")
    print(file.read())

    # Upload to S3
    s3 = boto3.resource('s3')
    s3.Bucket(bucket_name).upload_file(object_full_path, object_name_upload)
