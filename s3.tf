resource "aws_s3_bucket" "s3-output-bucket" {
  bucket = "assessment-jumbo-output-bucket"
  force_destroy = true

  tags = {
    Name        = "Assessment Jumbo output bucket"
    Environment = "Testing"
  }
}

resource "aws_s3_bucket_acl" "output_bucket_acl" {
  bucket = aws_s3_bucket.s3-output-bucket.id
  acl    = "private"
}

resource "aws_s3_bucket_server_side_encryption_configuration" "ssenc" {
  bucket = aws_s3_bucket.s3-output-bucket.bucket

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

locals {
  s3_origin_id = "myS3Origin"
}

data "aws_iam_policy_document" "cfn-origin-policy" {
	statement {
		actions = ["s3:GetObject"]

		resources = ["${aws_s3_bucket.s3-output-bucket.arn}/*"]

		principals {
			type        = "Service"
			identifiers = ["cloudfront.amazonaws.com"]
		}
		condition {
			test     = "StringEquals"
			variable = "AWS:SourceArn"
			values   = [aws_cloudfront_distribution.s3_distribution.arn]
		}
	}
}

resource "aws_s3_bucket_policy" "example" {
  bucket = aws_s3_bucket.s3-output-bucket.id
  policy = data.aws_iam_policy_document.cfn-origin-policy.json
}