############### AWS #################
variable "aws_region" {
  type        = string
  description = "The default AWS region to deploy"
  default     = "eu-west-1"
}

variable "aws_access_key" {
  type        = string
  sensitive   = true
  description = "The AWS access key to access AWS"
}

variable "aws_secret_key" {
  type        = string
  sensitive   = true
  description = "The AWS secret key to access AWS"
}
############### AWS #################

############### Variables for the application #################
variable "name" {
  type = string
  description = "Provide a name"
  default = "Yoël"
}

variable "age" {
  type = number
  description = "Provide a age"
  default = 99
}

variable "city" {
  type = string
  description = "provide a city"
  default = "Veghel"
}
############### Variables for the application #################